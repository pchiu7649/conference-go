import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Define parameters for api call
    params = {"per_page": 1, "query": f"{city} {state}"}
    # define url
    url = "https://api.pexels.com/v1/search"
    # Make the request
    response = requests.get(url, params=params, headers=headers)
    # Parse the JSON response
    content = json.loads(response.content)
    try:

        # Return a dictionary that contains a `picture_url` key and
        #   one of the URLs for one of the pictures in the response
        photo = {"picture_url": content["photos"][0]["src"]["original"]}
        return photo
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create a dictionary for the headers to use in the request

    # Define paramenters for api call
    params = {"q": f"{city},{state}", "appid": OPEN_WEATHER_API_KEY}
    # Create the URL for the geocoding API with the city and state
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    # Make the request
    response = requests.get(geo_url, params=params)
    # Parse the JSON response
    content = json.loads(response.content)

    # Get the latitude and longitude from the response
    latitude = content[0]["lat"]
    longitude = content[0]["lon"]

    # Set parameters for weather api call
    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
    }
    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    # Make the request
    response = requests.get(weather_url, params=weather_params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # try:
    weather = {
        "temperature": content["main"]["temp"],
        "weather_description": content["weather"][0]["description"],
    }
    # Return the dictionary
    return weather
    # except (KeyError, IndexError):
    #    return {
    #        "temperature": None,
    #       "weather_description": None,
    #    }
